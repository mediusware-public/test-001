<?php

return [
    'pms' => [
        'client_key' => env('PMS_CLIENT_KEY', ''),
        'patient_key' => env('PMS_PATIENT_KEY', ''),
        'end_point' => env('PMS_END_POINT', '')
    ]
];
