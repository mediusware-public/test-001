<?php

use App\Http\Controllers\AppointmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1/pms'], function(){
    Route::get('/read-patient', 'PmsController@readPatient')->name('readPatient');;
    Route::post('/create-appointment', 'PmsController@createAppointment')->name('createAppointment');
});
