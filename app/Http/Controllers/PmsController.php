<?php

namespace App\Http\Controllers;

use App\Http\Request\CreateAppointmentRequestValidation;
use App\Repositories\Interfaces\PmsInterface;
use Illuminate\Http\JsonResponse;

class PmsController extends Controller
{
    /**
     * @var PmsInterface
     */
    private $pmsRepository;

    /**
     * @param PmsInterface $pmsRepository
     */
    public function __construct(PmsInterface $pmsRepository) {
        $this->pmsRepository = $pmsRepository;
    }

    /**
     * @return JsonResponse
     */
    public function readPatient()
    {
        $patientKey = config('rest.pms.patient_key');
        $readPatient = $this->pmsRepository->read_patient($patientKey);
        return response()->json($readPatient);
    }

    public function createAppointment(CreateAppointmentRequestValidation $request)
    {
        try {
            $item = $this->pmsRepository->create_appointment($request->patientID, $request->appointmentDate);
            return response()->json($item);
        } catch (\Exception $e) {
            return response()->json($e);
        }

    }
}
