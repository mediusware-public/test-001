<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PmsInterface;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class PmsRepository implements PmsInterface
{
    /**
     * Fetch data via REST
     * @param $patientKey
     * @return \Exception|RequestException|Response
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function read_patient($patientKey)
    {
        $clientKey = config('rest.pms.client_key');
        $baseUrl = config('rest.pms.end_point');
        $url = $baseUrl . "/v2/patient?clientKey=$clientKey&patientKey=$patientKey";
        return Http::get($url)->throw()->json();
    }

    /**
     * Create Appointment key
     * @return string
     */
    public function  create_appointment_key() {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[-\+]';
        return substr(str_shuffle($permitted_chars), 0, 35);
    }

    /**
     * Store data via REST
     *
     * @param $patientID
     * @param $appointmentDate
     * @return \Exception|RequestException|Response
     * @throws \Illuminate\Http\Client\RequestException
     * @return Response
     */
    public function create_appointment($patientID, $appointmentDate)
    {
        $clientKey = config('rest.pms.client_key');
        $baseUrl = config('rest.pms.end_point');
        $appointmentKey = $this->create_appointment_key();
        $appointmentData = array('patientID' => $patientID, 'appointmentKey' => $appointmentKey, 'appointmentDate' => $appointmentDate);
        $appointmentData = json_encode($appointmentData);
        $url = $baseUrl . "/v2/appointment";

        return Http::post($url, [
            'clientKey' => $clientKey,
            'appointments' => [
                $appointmentData
            ]
        ])->throw()->json();
    }
}
