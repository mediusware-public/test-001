<?php
namespace App\Repositories\Interfaces;

interface PmsInterface
{
    /**
     * @param $patient_key
     */
    public function read_patient($patient_key);

    /**
     * @param $patient_id
     * @param $appointment_datetime
     */
    public function create_appointment($patient_id, $appointment_datetime);
}
